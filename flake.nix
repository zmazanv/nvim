{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    systems.url = "github:nix-systems/default";
  };

  outputs = inputs: let
    perSystem = f:
      inputs.nixpkgs.lib.genAttrs (import inputs.systems) (
        system:
          f inputs.nixpkgs.legacyPackages.${system}
      );
  in {
    devShells = perSystem (import ./shell.nix);
  };
}
